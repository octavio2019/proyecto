<?php 

    require ('conexion.php');
    $query="SELECT kcveestado, onombre FROM estado ORDER BY onombre asc";
    $resultado = $mysqli->query($query);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Diseño 1</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">       
    <script lenguaje="jasvascritp" src="php/jquery-3.1.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

</head>

<body>
    <div class="container">
        <div class="table-responsive table-bordered">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="5">Datos de Facturación</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td><label style="clear: both;display: block;"><span>*</span>RFC</label>
                        <input type="text" id="orfc" class="form-control form-control-sm"></td>
                        
                        <td colspan="4"><label style="clear: both;display: block;"><span>*</span>Razón Social</label>
                        <input type="text" id="orazonsocial" class="form-control form-control-sm"></td>
                        
                    </tr>
                    <tr>
                        <td><label style="clear: both;display: block;"><span>*</span>Email</label>
                            <input type="text" id="oemail" class="form-control form-control-sm"></td>
                        <td>
                            <label style="clear: both;display: block;"><span>*</span>Forma de Pago</label>
                            <select id="sel2" class="form-control form-control-sm">
                                <option value="undefined" selected="">--Seleccionar--</option>
                                <option value="01">(01) Efectivo</option>
                                <option value="28">(28) Tarjeta de Débito</option>
                                <option value="02">(02) Cheque</option>
                            </select>
                        </td>
                        <td>
                            <label style="clear: both;display: block;"><span>*</span>Número de Cuenta</label>
                            <input type="text" class="form-control form-control-sm"></td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td><label style="clear: both;display: block;"><span>*</span>Uso del CFDI</label>
                            <select class="form-control form-control-sm">
                            <option value="undefined" selected="">--Seleccionar Uso de CFDI</option>
                            <option value="G01">(G01) Adquisición de mercancias</option>
                            <option value="G03">(G03) Gastos en General</option></select>
                        </td>

                        <td><label style="clear: both;display: block;"><span>*</span>Método de Pago</label>
                            <select class="form-control form-control-sm"><option value="undefined" selected="">--Seleccionar Forma de Pago--</option>
                                <option value="PUE">(PUE) Pago en una sola exhibición</option>
                                <option value="PPD">(PPD) Pago en parcialidades o diferido</option></select>
                            </td>
                        <td
                            colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="5"><strong>Datos Opcionales</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <form id="combo" name="combo" method='POST'>
                            <label style="clear: both;display: block;">Estado</label>
                            <select id="cbx_kcveestado" name="cbx_kcveestado" class="form-control form-control-sm">
                                     
                                <option value="0">--Seleccionar--</option>                               
                                <?php while ($row = $resultado-> fetch_assoc() ){  ?>

                                <option value="<?php echo $row['kcveestado']; ?>">
                                <?php echo $row['onombre']; ?></option>
                                <?php }  ?>                                
                            </select>
                            
                            </td>

                        <td><label style="clear: both;display: block;">Ciudad/Municipio/Delegación</label>
                            <select id="cbx_municipio" name="cbx_municipio" class="form-control form-control-sm">
                            	 
                            </select>
                             </form>
                        </td>	
                        


                        <td><label style="clear: both;display: block;">Dirección</label>
                            <textarea id="odireccion"class="form-control form-control-sm"></textarea>
                        </td>
                        <td><label style="clear: both;display: block;">Colonia</label>
                            <input id="ocolonia"type="text" class="form-control form-control-sm">
                        </td>
                        <td><label style="clear: both;display: block;">Código Postal</label>
                            <input id="ocodigopostal"type="text" class="campoNumerico form-control form-control-sm">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive table-bordered table-striped" id="tablaConceptos">
            <table class="table">
                <thead>
                    <tr>
                        <th>Clave</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Costo Unitario</th>
                        <th>Descuento</th>
                        <th>IVA</th>
                        <th>Total</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><input type="text" class="campoNumerico form-control form-control-sm"></td>
                        <td><button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="text-align: center;">
            <button class="btn btn-info" id="botonAgregarUsuario" type="button" style="margin: auto;" data-toggle="modal" data-target="#modalnuevo"><i class="fa fa-plus-square-o" ></i>&nbsp;Agregar Usuario</button>

            <button class="btn btn-info" id="botonAgregarConcepto" type="button" style="margin: auto;" data-toggle="modal" data-target="#modalproducto"><i class="fa fa-plus-square-o"></i>&nbsp;Agregar Concepto</button>

       </div>


<!-- Modal usuario-->
<div class="modal fade" id="modalnuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Agregar usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>RFC</label>
            <input type="text" name="" id="rfc" class="form-control input">
            <label>Razon Social</label>
            <input type="text" name="" id="razonsocial" class="form-control input">
            <!--<label>Estado</label>
            <input type="text" name="" id="onombre" class="form-control input">
            <label>Municipio</label>
            <input type="text" name="" id="onombre1" class="form-control input">-->

    	<form id="combo" name="combo" method='POST'>
                    <label style="clear: both;display: block;">Estado</label>
                    <select id="cbx_kcveestado" name="cbx_kcveestado" class="form-control form-control-sm">                             
                        <option value="0" selected="">--Seleccionar--</option>                            
                        <?php 
   						 require ('conexion.php');
  						  $query="SELECT kcveestado, onombre FROM estado ORDER BY onombre asc";
  						  $resultado = $mysqli->query($query);
						?>                           
                        <?php while ($row = $resultado-> fetch_assoc() ){  ?>

                        <option value="<?php echo $row['kcveestado']; ?>">
                        <?php echo $row['onombre']; ?></option>
                        <?php }  ?>                                
                    </select>      

            <label style="clear: both;display: block;">Ciudad/Municipio/Delegación</label>
                            <select id="cbx_municipio" name="cbx_municipio" class="form-control form-control-sm">
                            	 <option value="0" selected="">--Seleccionar--</option>  

             </select>
             </form>


            <label>Direccion</label>
            <input type="text" name="" id="direccion" class="form-control input">
            <label>Colonia</label>
            <input type="text" name="" id="colonia" class="form-control input">
            <label>Codigo Postal</label>
            <input type="text" name="" id="postal" class="form-control input">

                
                <label for="validationDefaultUsername">Email</label>
                <div class="input-group">
                <div class="input-group-prepend">
                 <span class="input-group-text" id="inputGroupPrepend2">@</span>
                </div>
            <input type="text" class="form-control" id="email"  aria-describedby="inputGroupPrepend2" required>
          </div>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="guardarnuevo">Agregar Usuario</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal producto-->
<div class="modal fade" id="modalproducto" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Agregar Producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>Producto</label>
             <select class="form-control form-control-sm">
             <option value="" selected="">--Seleccionar Producto--</option>
 <?php 

   						 require ('conexion.php');
  						  $query="SELECT kcveproducto, onombre FROM productos ORDER BY onombre asc";
  						  $resultado = $mysqli->query($query);
						?>                           
                        <?php while ($row = $resultado-> fetch_assoc() ){  ?>

                        <option value="<?php echo $row['kcveproducto']; ?>">
                        <?php echo $row['onombre']; ?></option>
                        <?php }  ?>     

         </select>            
            
         
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Agregar Producto</button>
      </div>
    </div>
  </div>
</div>


        <div class="table-responsive" style="width: 300px;float: right;">

            <table class="table">
                <thead>
                    <tr></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Importe</td>
                        <td>$0.00</td>
                    </tr>
                    <tr>
                        <td>Descuento</td>
                        <td>$0.00</td>
                    </tr>
                    <tr>
                        <td>Subtotal</td>
                        <td>$0.00</td>
                    </tr>
                    <tr>
                        <td>IVA</td>
                        <td>$0.00</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>$0.00</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!--<script src="assets/js/jquery.min.js"></script>-->
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>

<script type="text/javascript">
    
     $(function() {
        $("#orfc").autocomplete({
                    source: "php/buscar.php",
                    minLength: 2,
                    select: function(event, ui) {
                        event.preventDefault();
                        console.log(ui);
                        $('#orfc').val(ui.item.orfc);
                        $('#orazonsocial').val(ui.item.orazonsocial);
                        $('#oemail').val(ui.item.oemail);
                        $('#odireccion').val(ui.item.odireccion);
                        $('#ocolonia').val(ui.item.ocolonia);
                        $('#ocodigopostal').val(ui.item.ocodigopostal);
                       


             
                     }
                });
    });

 
    </script>

    <script language="javascript">
			$(document).ready(function(){
				$("#cbx_kcveestado").change(function () {
 
					//$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#cbx_kcveestado option:selected").each(function () {
						kcveestado = $(this).val();

						$.post("php/municipio.php", { kcveestado: kcveestado}, function(data){
							console.log(data);
							$("#cbx_municipio").html(data);
						});            
					});
				})
			});
			</script>

		<!--<script type="text/javascript">
			$(document).ready(function(){
		$('#guardarnuevo').click(function(){
			orfc=$('#rfc').val();
			orazonsocial=$('#razonsocial').val();
			rcveestado=$('#cbx_kcveestado').val();
			rcvemunicipio=$('#cbx_municipio').val();
			odireccion=$('#direccion').val();
			ocolonia=$('#colonia').val();
			ocodigopostal=$('#postal').val();
			oemail=$('#email').val();
		
		insertar(orfc,orazonsocial,rcveestado,rcvemunicipio,odireccion,ocolonia,ocodigopostal,oemail)

	});-->
